<?php
    class Rectangle extends CI_Controller{
        public function show_form(){

            $this->load->view('rect');

        }
        public function get_data(){

            $form_data= $this->input->post();

/*
            $length=$this->input->post('length');

            $breadth=$this->input->post('breadth');

            $area= $length * $breadth ;
            $perimeter= 2* ($length + $breadth);

            echo "Area =" . $area;

            echo "<br>";

            echo "Perimeter =" . $perimeter;
            
            $data = array('area' => $area, 'perimeter' => $perimeter,'length' => $length, 'breadth' => $breadth );
*/

            $area = $form_data['length'] * $form_data['breadth'];
            $perimeter = 2* ($form_data['length'] + $form_data['breadth']);

            $data = array(
                            "length" => $form_data['length'], 
                            "breadth" => $form_data['breadth'],
                            "area" => $area,
                            "perimeter" => $perimeter
                        );

            $this->load->view('rect', $data);
        }

    }



?>