<!DOCTYPE html>
<html>
<head>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
table, th, td {
    border: 1px solid black;
}
</style>
</head>
<body class="container">


<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "bachelorsguide";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT `id`, `firstname`, `lastname`, `username`, `password`, `mobilephone`, `address`, `pincode`, `service`, 
`current email address`, `added_date` FROM `signup_sp`";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
	echo "<table class='table table-striped table-bordered table-hover table-condensed'><tr><th>ID</th><th>Name</th><th>username</th> <th>password</th> 
	<th>mobilephone</th> <th>address</th> <th>pincode</th> <th>service</th>
	<th>current email address</th><th>added_date</th>
	</tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
         echo "<tr><td>" . $row["id"]. "</td><td>" . $row["firstname"]. " " . $row["lastname"]."<td>"

		 . $row["username"]. "<td>" . $row["password"]. "<td>" . $row["mobilephone"]. "<td>" . $row["address"].
		 
		 "<td>" . $row["pincode"]. "<td>" . $row["service"]. "<td>" . $row["current email address"]. "<td>" . $row["added_date"].

		 "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

$conn->close();
?> 

</body>
</html>